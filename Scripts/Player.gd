extends Area2D

var screensize
export var speed = 300

onready var laser = preload("res://Scenes/Laser.tscn")

func _ready():
	screensize = get_viewport_rect().size

func _process(delta):
	var velocity = 0
	if Input.is_action_pressed("ui_right"):
		velocity = 1
	if Input.is_action_pressed("ui_left"):
		velocity = -1
	
	velocity = velocity * speed * delta
	position.x += velocity
	position.x = clamp(position.x, 0, screensize.x)
	
	if Input.is_action_just_pressed("ui_select"):
		var n_laser = laser.instance()
		n_laser.position = position
		n_laser.position.y -= 30
		get_node("/root/Main").add_child(n_laser)